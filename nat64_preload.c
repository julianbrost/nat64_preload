#define _GNU_SOURCE

#include <dlfcn.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>

int socket(int domain, int type, int protocol) {
	if (domain == AF_INET) {
		domain = AF_INET6;
	}
	int (*original_socket) (int, int, int);
	original_socket = dlsym(RTLD_NEXT, "socket");
	return (*original_socket)(domain, type, protocol);
}

int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
	struct sockaddr_in6 addr_in6 = { .sin6_family = AF_INET6 };
	if (addr->sa_family == AF_INET) {
		struct sockaddr_in *addr_in4 = (struct sockaddr_in *) addr;

		uint8_t nat64_prefix[12] = {0x00, 0x64, 0xff, 0x9b}; // 64:ff9b::/96
		memcpy(addr_in6.sin6_addr.s6_addr, nat64_prefix, 12);
		memcpy(addr_in6.sin6_addr.s6_addr + 12, &addr_in4->sin_addr, 4);

		addr_in6.sin6_port = addr_in4->sin_port;

		addr = (struct sockaddr *) &addr_in6;
		addrlen = sizeof(addr_in6);
	}
	int (*original_connect) (int, const struct sockaddr *, socklen_t);
	original_connect = dlsym(RTLD_NEXT, "connect");
	return (*original_connect)(sockfd, addr, addrlen);
}

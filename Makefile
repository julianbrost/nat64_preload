all: nat64_preload.so

clean:
	$(RM) nat64_preload.so

nat64_preload.so: nat64_preload.c
	$(CC) -std=c99 -shared -fPIC -o $@ $<

.PHONY: all clean
